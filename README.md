# Optimisation des performances de VSCode
Lancez VS Code.
Allez dans Fichier > Préférences > Paramètres : dans la fenêtre d’édition de droite, ajoutez entre les accolades :
```bash
"telemetry.enableTelemetry": false
```

# Raccourci clavier pour exécuter un script Python dans VSCode
Aller dans Fichier > Préférences > Raccourcies clavier
Chercher "Exécuter le script Python dans un terminal"
Faire un clic-droit sur le premier résultat et sélectionner "Ajouter une combinaison de touches"
Entrer la combinaison de touches : CTRL + R

Source:
https://code.visualstudio.com/docs/getstarted/keybindings

# Proxy ENSAI

## Git bash
```
git config --global http.proxy http://pxcache-02.ensai.fr:3128
git config --global https.proxy http://pxcache-02.ensai.fr:3128
```

## Pip
```
pip install psycopg2-binary --user --proxy http://pxcache-02.ensai.fr:3128
pip install -r requirements.txt --user --proxy http://pxcache-02.ensai.fr:3128
```